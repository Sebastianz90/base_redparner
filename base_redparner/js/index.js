$(function() {
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
            interval: 4000
        });
        $("#exampleModal").on('show.bs.modal', function(e) {
            console.log('el Modal se está mostrando');
            $("#contactoBtn").removeClass('btn-success');
            $("#contactoBtn").addClass('btn-secondary');
            $("#contactoBtn").prop("disabled", true);
        });
        $("#exampleModal").on('shown.bs.modal', function(e) {
            console.log('el Modal se mostró');
        });
        $("#exampleModal").on('hide.bs.modal', function(e) {
            console.log('el Modal se oculta');
        });
        $("#exampleModal").on('hidden.bs.modal', function(e) {
            console.log('el Modal se ocultó');
            $("#contactoBtn").prop('disabled', false);
            $("#contactoBtn").removeClass('btn-secondary');
            $("#contactoBtn").addClass('btn-success');
        });
    });