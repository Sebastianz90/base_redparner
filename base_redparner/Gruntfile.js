module.exports = function(grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });
    grunt.initConfig({
            sass: {
                dist: {
                    files: [{
                        expand: true,
                        cwd: 'stylesheet',
                        src: ['*.scss'],
                        dest: 'stylesheet',
                        ext: '.css'

                    }]
                }

            },
            watch: { //tarea permanente que observa los cambios en el archivo .scss
                files: ['stylesheet/*.scss'],
                tasks: ['css']
            },

            browserSync: {
                dev: {
                    bsfiles: { //Archivos que examina el browser
                        src: [
                            'css/*.css',
                            '*.html',
                            'js/*.js'
                        ]
                    },
                    options: {
                        watchTask: true,
                        server: {
                            baseDir: './' //Directorio base del servidor
                        }
                    }
                }
            },

            imagemin: {
                dynamic: {
                    files: [{
                        expand: true,
                        cwd: './',
                        src: 'img/*.{png,gif,jpg,jpeg}',
                        dest: 'dist'
                    }]
                }
            },

            copy: {
                html: {
                    files: [{
                        expand: true,
                        cwd: './',
                        src: ['*html'],
                        dest: 'dist'
                    }]

                },

            },

            fonts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/get-google-fonts/',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            

        },

        clean: {
            build: {
                src: ['dist/']
            }
        },

        cssmin: {
            dist: {}
        },


        uglify: {
            dist: {}

        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                leght: 20
            },

            release: {
                files: [{
                    src: [
                        'dist/js/.*js',
                        'dist/css/.*css',

                    ]
                }]
            }
        },

        concat: {
            options: {
                separator: ';'

            },
            dist: {}
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html, nosotros.html , precios.html , contacto.html']
            },

            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']

                    },

                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false

                                }

                            }

                        }]

                    }

                }

            }

        },

        usemin: {
            html: ['dist/index.html', 'dist/nosotros.html', 'dist/precios.html', 'dist/contacto.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }

        }

    });

grunt.registerTask('css', ['sass']);
grunt.registerTask('default', ['browserSync', 'watch']);
grunt.registerTask('img:compress', ['imagemin']);
grunt.registerTask('build', [
    'clean',
    'copy',
    'imagemin',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
])
};